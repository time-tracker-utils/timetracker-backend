package yaklet.webserver.endpointController.authService

import dev.yakstack.transport.Login
import org.junit.Test
import yaklet.webserver.endpointController.AbstractDatabaseTest
import java.util.*

class LogoutControllerTest: AbstractDatabaseTest() {

    @Test
    fun testValidToken() {
        val conn = provideConn()
        val logoutController = LogoutController()

        val result = logoutController.process(
            conn,
            Login.Token.newBuilder()
                .setToken(UUID.randomUUID().toString())
                .build()
        )

        assert(result.result == LogoutResult.OK)
    }

    @Test
    fun testInvalidToken() {
        val conn = provideConn()
        val logoutController = LogoutController()

        val result = logoutController.process(
            conn,
            Login.Token.newBuilder()
                .setToken("hey there fellas")
                .build()
        )

        assert(result.result == LogoutResult.INVALID_TOKEN)
    }
}
