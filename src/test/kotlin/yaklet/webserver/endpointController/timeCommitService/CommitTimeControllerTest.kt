package yaklet.webserver.endpointController.timeCommitService

import dev.yakstack.transport.Timecommit
import org.junit.Test
import yaklet.webserver.endpointController.AbstractDatabaseTest
import java.util.*

class CommitTimeControllerTest: AbstractDatabaseTest() {
    @Test
    fun canSubmitNewCommits() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)

        val commitTimeResult = CommitTimeController()
            .process(
                conn,
                getTimeCommitBundle(setupPair.first, timeCommits)
            )
        assert(commitTimeResult.result == TimeCommitIdResult.OK)
    }

    @Test
    fun cannotSubmitWithBadToken() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val commitTimeController = CommitTimeController()

        val commitTimeMalformed = commitTimeController.process(
            conn,
            getTimeCommitBundle(
                "glarriendasser.com",
                getTimeCommits(setupPair.first)
            )
        )

        assert(commitTimeMalformed.result == TimeCommitIdResult.INVALID_TOKEN)

        val commitTimeNonexistent = commitTimeController.process(
            conn,
            getTimeCommitBundle(
                UUID.randomUUID().toString(),
                getTimeCommits(setupPair.first)
            )
        )
        assert(commitTimeNonexistent.result == TimeCommitIdResult.INVALID_TOKEN)
    }

    @Test
    fun cannotSubmitMissingFields() {
        val conn = provideConn()
        val commitTimeController = CommitTimeController()

        val commitTimeResult = commitTimeController.process(
            conn,
            getTimeCommitBundle(
                "glarriendasser.com",
                listOf(
                    Timecommit.TimeCommit.newBuilder()
                    .setStartTime(1000000)
                    .build()
                )
            )
        )

        assert(commitTimeResult.result == TimeCommitIdResult.MISSING_FIELDS)
    }

    @Test
    fun cannotSubmitFutureInPast() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val commitTimeController = CommitTimeController()

        val commitTimeResult = commitTimeController.process(
            conn,
            getTimeCommitBundle(
                "glarriendasser.com",
                listOf(
                    Timecommit.TimeCommit.newBuilder()
                        .setTaskId(setupPair.first)
                        .setStartTime(1000000)
                        .setStartTz("America/New_York")
                        .setEndTime(5)
                        .setEndTz("America/New_York")
                        .build()
                )
            )
        )

        assert(commitTimeResult.result == TimeCommitIdResult.INVALID_TIME_RANGE)
    }

    @Test
    fun cannotSubmitBadTimeZone() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val commitTimeController = CommitTimeController()

        val commitTimeResult = commitTimeController.process(
            conn,
            getTimeCommitBundle(
                "glarriendasser.com",
                listOf(
                    Timecommit.TimeCommit.newBuilder()
                        .setTaskId(setupPair.first)
                        .setStartTime(10)
                        .setStartTz("davemachado.com")
                        .setEndTime(10000)
                        .setEndTz("America/New_York")
                        .build()
                )
            )
        )

        assert(commitTimeResult.result == TimeCommitIdResult.INVALID_TIME_ZONE)
    }

    @Test
    fun cannotSubmitBadTaskId() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val commitTimeController = CommitTimeController()

        val malformedTaskIdResult = commitTimeController.process(
            conn,
            getTimeCommitBundle(
                setupPair.first,
                listOf(
                    Timecommit.TimeCommit.newBuilder()
                        .setTaskId("dog")
                        .setStartTime(10)
                        .setStartTz("America/New_York")
                        .setEndTime(10000)
                        .setEndTz("America/New_York")
                        .build()
                )
            )
        )

        assert(malformedTaskIdResult.result == TimeCommitIdResult.INVALID_TASK_ID)

        val nonexistentTaskIdResult = commitTimeController.process(
            conn,
            getTimeCommitBundle(
                setupPair.first,
                listOf(
                    Timecommit.TimeCommit.newBuilder()
                        .setTaskId(UUID.randomUUID().toString())
                        .setStartTime(10)
                        .setStartTz("America/New_York")
                        .setEndTime(10000)
                        .setEndTz("America/New_York")
                        .build()
                )
            )
        )

        assert(nonexistentTaskIdResult.result == TimeCommitIdResult.INVALID_TASK_ID)
    }


    private fun getTimeCommitBundle(token: String, timeCommitList: List<Timecommit.TimeCommit>): Timecommit.TimeCommitBundle =
        Timecommit.TimeCommitBundle.newBuilder()
            .setToken(token)
            .addAllCommits(timeCommitList)
            .build()

    private fun getTimeCommits(taskId: String) = listOf(
        Timecommit.TimeCommit.newBuilder()
            .setTaskId(taskId)
            .setStartTime(909090)
            .setStartTz("America/North_Dakota/New_Salem")
            .setEndTime(10101010)
            .setEndTz("America/Port-au-Prince")
            .build(),
        Timecommit.TimeCommit.newBuilder()
            .setTaskId(taskId)
            .setStartTime(100000)
            .setStartTz("America/New_York")
            .setEndTime(11000000)
            .setEndTz("America/New_York")
            .build()
    )
}
