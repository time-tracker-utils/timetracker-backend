package yaklet.webserver.endpointController.timeCommitService

import dev.yakstack.transport.TaskOuterClass
import dev.yakstack.transport.Timecommit
import org.junit.Test
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.endpointController.AbstractDatabaseTest
import java.util.*

class GetTimeCommitControllerTest: AbstractDatabaseTest() {

    @Test
    fun canGetTimeCommits() {
        val conn = provideConn()
        val getTimeCommitController = GetTimeCommitController()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)
        addSomeTimeCommits(conn, getTimeCommitBundle(setupPair.first, timeCommits))

        val getTimeCommitResult = getTimeCommitController.process(
            conn,
            TaskOuterClass.TaskId.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setToken(setupPair.first)
                .build()
        )
        assert(getTimeCommitResult.result == TimeCommitBundleResult.OK)
        assert(getTimeCommitResult.timeCommits.commitsList.size == timeCommits.size)
        getTimeCommitResult.timeCommits.commitsList.zip(timeCommits).forEach {
            println(it.first)
            println(it.second)
            assert(it.first.startTime == it.second.startTime)
            assert(it.first.startTz == it.second.startTz)
            assert(it.first.endTime == it.second.endTime)
            assert(it.first.endTz == it.second.endTz)
        }
    }

    @Test
    fun cannotGetWithBadToken() {
        val conn = provideConn()
        val getTimeCommitController = GetTimeCommitController()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)
        addSomeTimeCommits(conn, getTimeCommitBundle(setupPair.first, timeCommits))

        var getTimeCommitResult = getTimeCommitController.process(
            conn,
            TaskOuterClass.TaskId.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setToken("glarrienglasser.com")
                .build()
        )
        assert(getTimeCommitResult.result == TimeCommitBundleResult.INVALID_TOKEN)


        getTimeCommitResult = getTimeCommitController.process(
            conn,
            TaskOuterClass.TaskId.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setToken(UUID.randomUUID().toString())
                .build()
        )
        assert(getTimeCommitResult.result == TimeCommitBundleResult.INVALID_TOKEN)
    }

    @Test
    fun cannotGetWithBadTaskId() {
        val conn = provideConn()
        val getTimeCommitController = GetTimeCommitController()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)
        addSomeTimeCommits(conn, getTimeCommitBundle(setupPair.first, timeCommits))

        var getTimeCommitResult = getTimeCommitController.process(
            conn,
            TaskOuterClass.TaskId.newBuilder()
                .setTaskId("glarrienglasser.com")
                .setToken(setupPair.first)
                .build()
        )
        assert(getTimeCommitResult.result == TimeCommitBundleResult.INVALID_TASK_ID)

        getTimeCommitResult = getTimeCommitController.process(
            conn,
            TaskOuterClass.TaskId.newBuilder()
                .setTaskId(UUID.randomUUID().toString())
                .setToken(setupPair.first)
                .build()
        )
        assert(getTimeCommitResult.result == TimeCommitBundleResult.INVALID_TASK_ID)
    }

    private fun addSomeTimeCommits(conn: DbConn, timeCommits: Timecommit.TimeCommitBundle) {
        val commitTimeResult = CommitTimeController().process(conn, timeCommits)
        assert(commitTimeResult.result == TimeCommitIdResult.OK)
    }

    private fun getTimeCommitBundle(token: String, timeCommitList: List<Timecommit.TimeCommit>): Timecommit.TimeCommitBundle =
        Timecommit.TimeCommitBundle.newBuilder()
            .setToken(token)
            .addAllCommits(timeCommitList)
            .build()

    private fun getTimeCommits(taskId: String) = listOf(
        Timecommit.TimeCommit.newBuilder()
            .setTaskId(taskId)
            .setStartTime(909090)
            .setStartTz("America/North_Dakota/New_Salem")
            .setEndTime(10101010)
            .setEndTz("America/Port-au-Prince")
            .build(),
            Timecommit.TimeCommit.newBuilder()
            .setTaskId(taskId)
            .setStartTime(100000)
            .setStartTz("America/New_York")
            .setEndTime(11000000)
            .setEndTz("America/New_York")
            .build()
    )
}
