package yaklet.webserver.endpointController

import org.junit.Rule
import org.testcontainers.containers.PostgreSQLContainer
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.dbConn.MockConnectionProvider
import yaklet.webserver.dbConn.dslFactory.PostgresDSLFactory

abstract class AbstractDatabaseTest {
    @get:Rule
    public var pg = PostgreSql11Container()

    protected fun provideConn(): DbConn {
        val mockConnectionProvider = MockConnectionProvider(
            pg.jdbcUrl,
            pg.username,
            pg.password
        )

        mockConnectionProvider.initTables()
        return DbConn(PostgresDSLFactory(mockConnectionProvider.conn))
    }
}

public class PostgreSql11Container(): PostgreSQLContainer<PostgreSql11Container>("postgres:11.1")
