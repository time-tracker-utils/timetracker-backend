package yaklet.webserver.endpointController.taskService

import dev.yakstack.transport.Login
import dev.yakstack.transport.Register
import dev.yakstack.transport.TaskOuterClass
import org.junit.Test
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.endpointController.AbstractDatabaseTest
import yaklet.webserver.endpointController.authService.LoginController
import yaklet.webserver.endpointController.authService.LoginResult
import yaklet.webserver.endpointController.authService.RegisterController

class DeleteTaskControllerTest: AbstractDatabaseTest() {

    @Test
    fun deletesCorrectly() {
        val conn = provideConn()
        val deleteTaskController = DeleteTaskController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()
        val taskIdBundle = submitTasks(
            conn,
            getTaskBundle(token, taskList)
        )

        val taskToDelete = taskIdBundle.taskIdsList[0].taskId
        val deleteTaskResult = deleteTaskController.process(
            conn,
            TaskOuterClass.TaskId
                .newBuilder()
                .setTaskId(taskToDelete)
                .setToken(token)
                .build()
        )

        assert(deleteTaskResult.result == DeleteTaskResult.OK)
        getTasksFromDb(conn, token).tasksList.forEach {
            assert(it.taskId != taskToDelete)
        }
    }

    @Test
    fun needsToken() {
        val conn = provideConn()
        val deleteTaskController = DeleteTaskController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()
        val taskIdBundle = submitTasks(
            conn,
            getTaskBundle(token, taskList)
        )

        // Do not set token
        val taskToDelete = TaskOuterClass.TaskId.newBuilder()
            .setTaskId(taskIdBundle.taskIdsList[0].taskId)
            .build()

        val deleteTaskResult = deleteTaskController.process(conn, taskToDelete)
        assert(deleteTaskResult.result == DeleteTaskResult.INVALID_TOKEN)
    }

    private fun getTaskList() = listOf(
            TaskOuterClass.Task.newBuilder()
            .setCategory("Testing weee")
            .setState("IN-PROGRESS")
            .setTask("Write some tests")
            .setNotes("blah blah blah I guess we need tests")
            .build(),
        TaskOuterClass.Task.newBuilder()
            .setCategory("Testing weee")
            .setState("TODO")
            .setTask("Write tests for this whole gosh darn application")
            .setNotes("Boy do I wish these wrote themselves")
            .build(),
        TaskOuterClass.Task.newBuilder()
            .setCategory("Something actually fun")
            .setState("DONE")
            .setTask("Give Vrinda a hug")
            .setNotes("She is a bean")
            .build()
    )

    private fun getTaskBundle(token: String, taskList: List<TaskOuterClass.Task>) = TaskOuterClass.TaskBundle.newBuilder()
        .setToken(token)
        .addAllTasks(taskList)
        .build()

    private fun setupUserAndLogin(conn: DbConn): String {
        val registerController = RegisterController()
        val loginController = LoginController()

        val email = "glarrien@darrien.com"
        val password = "hungrybubber159"

        registerController.process(
            conn,
            Register.RegisterRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setFirstName("glarrien")
                .setLastName("dasser")
                .build()
        )

        val loginResult = loginController.process(
            conn,
            Login.LoginRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setDeviceName("some kinda test device idk")
                .build()
        )

        assert(loginResult.result == LoginResult.OK)

        return loginResult.token.token
    }

    private fun submitTasks(conn: DbConn, taskBundle: TaskOuterClass.TaskBundle): TaskOuterClass.TaskIdBundle {
        val taskBundleResult = SubmitTaskController().process(conn, taskBundle)
        assert(taskBundleResult.result == TaskIdBundleResult.OK)

        return taskBundleResult.idBundle
    }

    private fun getTasksFromDb(conn: DbConn, token: String): TaskOuterClass.TaskBundle {
        val getTasksResult = GetTasksController().process(conn, Login.Token
            .newBuilder()
            .setToken(token)
            .build()
        )

        assert(getTasksResult.result == TaskBundleResult.OK)

        return getTasksResult.taskBundle
    }
}
