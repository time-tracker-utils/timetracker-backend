package yaklet.webserver.endpointController.taskService

import dev.yakstack.transport.Login
import dev.yakstack.transport.Register
import dev.yakstack.transport.TaskOuterClass
import org.junit.Test
import yaklet.webserver.config.MAX_NOTE_SIZE
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.endpointController.AbstractDatabaseTest
import yaklet.webserver.endpointController.authService.LoginController
import yaklet.webserver.endpointController.authService.LoginResult
import yaklet.webserver.endpointController.authService.RegisterController
import java.util.*

class SubmitTaskControllerTest: AbstractDatabaseTest() {

    @Test
    fun testCanSubmitTasks() {
        val conn = provideConn()
        val submitTaskController = SubmitTaskController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()

        val taskBundle = getTaskBundle(token, taskList)

        val submitTaskResult = submitTaskController.process(conn, taskBundle)
        assert(submitTaskResult.result == TaskIdBundleResult.OK)
        assert(taskList.size == submitTaskResult.idBundle.taskIdsList.size)
        submitTaskResult.idBundle.taskIdsList.forEach {
            UUID.fromString(it.taskId)
        }
    }

    @Test
    fun testInvalidToken() {
        val conn = provideConn()
        val submitTaskController = SubmitTaskController()

        val token = setupUserAndLogin(conn)

        val taskBundle = getTaskBundle(token + "invalid", getTaskList())
        val submitTaskResult = submitTaskController.process(conn, taskBundle)

        assert(submitTaskResult.result == TaskIdBundleResult.INVALID_TOKEN)
    }

    @Test
    fun testMissingFields() {
        val conn = provideConn()
        val submitTaskController = SubmitTaskController()

        val token = setupUserAndLogin(conn)

        val submitTaskResult = submitTaskController.process(
            conn,
            TaskOuterClass.TaskBundle.newBuilder()
                .setToken(token)
                .addAllTasks(
                    listOf(
                        TaskOuterClass.Task.newBuilder()
                            .setNotes("lol this is missing every field")
                            .build()
                    )
                ).build()
        )

        assert(submitTaskResult.result == TaskIdBundleResult.MISSING_FIELDS)
    }

    private fun getTaskList() = listOf(
            TaskOuterClass.Task.newBuilder()
                .setCategory("Testing weee")
                .setState("IN-PROGRESS")
                .setTask("Write some tests")
                .setNotes("blah blah blah I guess we need tests")
                .build(),
            TaskOuterClass.Task.newBuilder()
                .setCategory("Testing weee")
                .setState("TODO")
                .setTask("Write tests for this whole gosh darn application")
                .setNotes("Boy do I wish these wrote themselves")
                .build(),
            TaskOuterClass.Task.newBuilder()
                .setCategory("Something actually fun")
                .setState("DONE")
                .setTask("Give Vrinda a hug")
                .setNotes(Random().ints(MAX_NOTE_SIZE * 2L).toString())
                .build()
        )

    private fun getTaskBundle(token: String, taskList: List<TaskOuterClass.Task>) = TaskOuterClass.TaskBundle.newBuilder()
            .setToken(token)
            .addAllTasks(taskList)
            .build()

    private fun setupUserAndLogin(conn: DbConn): String {
        val registerController = RegisterController()
        val loginController = LoginController()

        val email = "glarrien@darrien.com"
        val password = "hungrybubber159"

        registerController.process(
            conn,
            Register.RegisterRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setFirstName("glarrien")
                .setLastName("dasser")
                .build()
        )

        val loginResult = loginController.process(
            conn,
            Login.LoginRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setDeviceName("some kinda test device idk")
                .build()
        )

        assert(loginResult.result == LoginResult.OK)

        return loginResult.token.token
    }

}
