package yaklet.webserver.dbConn

import org.junit.Test
import yaklet.webserver.dbConn.connectionFactory.MockConnectionFactory
import java.lang.Thread.sleep

class ConnectionPoolTest {
    /**
     * Ensure we act appropriately if someone tries to get more than the max amount
     * of connections.
     */
    @Test fun testMaxConnectionsRespected() {
        val pool = ConnectionPool(5, MockConnectionFactory())

        for (x in 0..4) {
            pool.getConnection()
        }

        // Should throw an exception when asking for more than the max connections
        var threwError = false
        try {
            pool.getConnection()
        } catch (_: OutOfConnectionsException) {
            threwError = true
        }

        assert(threwError)
    }

    @Test fun testRegularConnectionUsage() {
        val pool = ConnectionPool(5, MockConnectionFactory())
        val connectionList = mutableListOf<DbConn>()

        for (x in 0..2) {
            connectionList.add(pool.getConnection())
        }

        for (conn in connectionList) {
            pool.returnConnection(conn)
        }

        val conn = pool.getConnection()
        pool.returnConnection(conn)

        for (x in 0..4) {
            connectionList.add(pool.getConnection())
            pool.returnConnection(connectionList[0])
        }
    }

    @Test fun testConnectionPoolRespectsShutdown() {
        val pool = ConnectionPool(100, MockConnectionFactory())

        val connectionList = mutableListOf<DbConn>()

        for (x in 0..45) {
            connectionList.add(pool.getConnection())
        }

        Thread {
            pool.shutdown(3000)
        }.start()

        // More than enough time for the thread to kick off.
        sleep(500)

        // Try to get another connection now that it's dead.
        var threwError = false
        try {
            pool.getConnection()
        } catch (e: ShutdownModeException) {
            threwError = true
        }
        assert(threwError)
    }
}
