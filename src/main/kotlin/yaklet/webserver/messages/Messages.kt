package yaklet.webserver.messages

// Messages
const val SHUTDOWN_MODE_MSG = "Yaklet webserver is in shutdown mode. Accepting no more requests."

// Errors
const val OUT_OF_CONNECTIONS_ERROR = "Database currently overloaded. Please try again in a moment."

