package yaklet.webserver.endpointController.timeCommitService

import dev.yakstack.transport.Timecommit
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import yaklet.webserver.dbConn.DB_TIME_COMMIT_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.TaskHandler
import yaklet.webserver.handlers.TimeCommitHandler
import yaklet.webserver.handlers.TokenHandler
import java.sql.Timestamp
import java.util.*

class CommitTimeController {
    private val timeCommitHandler = TimeCommitHandler()
    private val tokenHandler = TokenHandler()
    private val taskHandler = TaskHandler()

    fun process(conn: DbConn, request: Timecommit.TimeCommitBundle): CommitTimeCommitsResult {
        return when {
            timeCommitHandler.missingCriticalFields(request) -> CommitTimeCommitsResult(
                TimeCommitIdResult.MISSING_FIELDS,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            !timeCommitHandler.timesAreValid(request) -> CommitTimeCommitsResult(
                TimeCommitIdResult.INVALID_TIME_RANGE,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            !timeCommitHandler.timeZonesAreValid(request) -> CommitTimeCommitsResult(
                TimeCommitIdResult.INVALID_TIME_ZONE,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            !tokenHandler.tokenIsValid(conn, request.token) -> CommitTimeCommitsResult(
                TimeCommitIdResult.INVALID_TOKEN,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            !taskHandler.commitsHaveValidTaskIds(conn, request) -> CommitTimeCommitsResult(
                TimeCommitIdResult.INVALID_TASK_ID,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            else -> CommitTimeCommitsResult(
                TimeCommitIdResult.OK,
                commitTimeCommits(conn, request, generateTimeCommitIdBundle(request))
            )
        }
    }

    private fun generateTimeCommitIdBundle(request: Timecommit.TimeCommitBundle) = request
        .commitsList
        .map { UUID.randomUUID() }

    private fun commitTimeCommits(
        conn: DbConn,
        request: Timecommit.TimeCommitBundle,
        timeCommitIdList: List<UUID>
    ): Timecommit.TimeCommitIdBundle {
        // Ensure we are working with data-sets of the same size
        assert(request.commitsList.size == timeCommitIdList.size)

        conn.sql.insertInto(
            table(DB_TIME_COMMIT_TB),
            field("time_id"),
            field("task_id"),
            field("start_time"),
            field("start_tz"),
            field("end_time"),
            field("end_tz")
        ).apply {
            for((idx, timeCommit) in request.commitsList.withIndex()) {
                values(
                    timeCommitIdList[idx],
                    UUID.fromString(timeCommit.taskId),
                    Timestamp(timeCommit.startTime),
                    timeCommit.startTz,
                    Timestamp(timeCommit.endTime),
                    timeCommit.endTz
                )
            }
        }.execute()

        return Timecommit.TimeCommitIdBundle
            .newBuilder()
            .addAllCommitIds(
                timeCommitIdList.map { Timecommit.TimeCommitId.newBuilder().setCommitId(it.toString()).build() }
            ).build()
    }
}

enum class TimeCommitIdResult {
    MISSING_FIELDS,
    INVALID_TASK_ID,
    INVALID_TOKEN,
    INVALID_TIME_RANGE,
    INVALID_TIME_ZONE,
    OK
}

data class CommitTimeCommitsResult(val result: TimeCommitIdResult, val idBundle: Timecommit.TimeCommitIdBundle)
