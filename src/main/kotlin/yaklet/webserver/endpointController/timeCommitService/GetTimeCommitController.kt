package yaklet.webserver.endpointController.timeCommitService

import dev.yakstack.transport.TaskOuterClass
import dev.yakstack.transport.Timecommit
import org.jooq.impl.DSL.field
import yaklet.webserver.dbConn.DB_TIME_COMMIT_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.TaskHandler
import yaklet.webserver.handlers.TokenHandler
import java.util.*

class GetTimeCommitController {
    private val tokenHandler = TokenHandler()
    private val taskHandler = TaskHandler()

    fun process(conn: DbConn, request: TaskOuterClass.TaskId): GetTimeCommitsResult {
        return when {
            !tokenHandler.tokenIsValid(conn, request.token) -> GetTimeCommitsResult(
                TimeCommitBundleResult.INVALID_TOKEN,
                Timecommit.TimeCommitBundle.getDefaultInstance()
            )
            !taskHandler.taskIdIsValid(conn, request.taskId) -> GetTimeCommitsResult(
                TimeCommitBundleResult.INVALID_TASK_ID,
                Timecommit.TimeCommitBundle.getDefaultInstance()
            )
            else -> GetTimeCommitsResult(
                TimeCommitBundleResult.OK,
                getTimeCommits(conn, request)
            )
        }
    }

    private fun getTimeCommits(conn: DbConn, request: TaskOuterClass.TaskId): Timecommit.TimeCommitBundle {
        val timeCommitRecords = conn.sql
            .select()
            .from(DB_TIME_COMMIT_TB)
            .where(field("task_id").eq(UUID.fromString(request.taskId)))
            .orderBy(field("start_time").desc())
            .fetch()

        return Timecommit.TimeCommitBundle.newBuilder().addAllCommits(
            timeCommitRecords.map {  timeCommitRecord ->
                Timecommit.TimeCommit.newBuilder()
                    .setTimeId(timeCommitRecord["time_id"].toString())
                    .setTaskId(timeCommitRecord["task_id"].toString())
                    .setStartTime(timeCommitRecord["start_time", Long::class.java])
                    .setStartTz(timeCommitRecord["start_tz"].toString())
                    .setEndTime(timeCommitRecord["end_time", Long::class.java])
                    .setEndTz(timeCommitRecord["end_tz"].toString())
                    .build()
            }
        ).build()
    }
}

enum class TimeCommitBundleResult {
    INVALID_TOKEN,
    INVALID_TASK_ID,
    OK
}

class GetTimeCommitsResult(val result: TimeCommitBundleResult, val timeCommits: Timecommit.TimeCommitBundle)
