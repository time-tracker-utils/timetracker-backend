package yaklet.webserver.endpointController.timeCommitService

import dev.yakstack.transport.Timecommit
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import yaklet.webserver.dbConn.DB_TIME_COMMIT_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.TokenHandler
import java.util.*

class DeleteTimeCommitController {
    private val tokenHandler = TokenHandler()

    fun process(conn: DbConn, request: Timecommit.TimeCommitId): DeleteTimeCommitResult {
        return when {
            !tokenHandler.tokenIsValid(conn, request.token) -> DeleteTimeCommitResult.INVALID_TOKEN
            !validTimeCommitId(request.commitId) -> DeleteTimeCommitResult.INVALID_COMMIT_ID
            else -> {
                deleteTimeCommit(conn, request)
                DeleteTimeCommitResult.OK
            }
        }
    }

    private fun deleteTimeCommit(conn: DbConn, request: Timecommit.TimeCommitId) {
        conn.sql.deleteFrom(table(DB_TIME_COMMIT_TB))
            .where(field("time_id").eq(request.commitId))
    }

    private fun validTimeCommitId(commitId: String): Boolean {
        return try {
            UUID.fromString(commitId)
            true
        } catch (_: IllegalArgumentException) { false }
    }
}

enum class DeleteTimeCommitResult {
    INVALID_TOKEN,
    INVALID_COMMIT_ID,
    OK
}
