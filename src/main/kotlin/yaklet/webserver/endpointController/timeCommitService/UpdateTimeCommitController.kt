package yaklet.webserver.endpointController.timeCommitService

import dev.yakstack.transport.Timecommit
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import yaklet.webserver.dbConn.DB_TIME_COMMIT_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.TimeCommitHandler
import yaklet.webserver.handlers.TokenHandler
import java.sql.Timestamp
import java.util.*

class UpdateTimeCommitController {
    private val timeCommitHandler = TimeCommitHandler()
    private val tokenHandler = TokenHandler()

    fun process(conn: DbConn, request: Timecommit.TimeCommitBundle): UpdateTimeCommitResult {
        return when {
            timeCommitHandler.missingCriticalFields(request) -> UpdateTimeCommitResult(
                UpdateTimeCommitIdResult.MISSING_FIELDS,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            !timeCommitHandler.timesAreValid(request) -> UpdateTimeCommitResult(
                UpdateTimeCommitIdResult.INVALID_TIME_RANGE,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            !timeCommitHandler.timeZonesAreValid(request) -> UpdateTimeCommitResult(
                UpdateTimeCommitIdResult.INVALID_TIME_ZONE,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            !tokenHandler.tokenIsValid(conn, request.token) -> UpdateTimeCommitResult(
                UpdateTimeCommitIdResult.INVALID_TOKEN,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            !timeCommitHandler.commitsHaveValidCommitIds(conn, request) -> UpdateTimeCommitResult(
                UpdateTimeCommitIdResult.INVALID_COMMIT_ID,
                Timecommit.TimeCommitIdBundle.getDefaultInstance()
            )
            else -> UpdateTimeCommitResult(
                UpdateTimeCommitIdResult.OK,
                updateTimeCommits(conn, request)
            )
        }
    }

    private fun updateTimeCommits(conn: DbConn, request: Timecommit.TimeCommitBundle): Timecommit.TimeCommitIdBundle =
        Timecommit.TimeCommitIdBundle.newBuilder()
            .addAllCommitIds(request.commitsList.map { commit ->
                    conn.sql.update(table(DB_TIME_COMMIT_TB))
                        .set(field("start_time"), Timestamp(commit.startTime))
                        .set(field("start_tz"), commit.startTz)
                        .set(field("end_time"), Timestamp(commit.endTime))
                        .set(field("end_tz"), commit.endTz)
                        .where(field("time_id").eq(UUID.fromString(commit.timeId)))
                        .execute()
                Timecommit.TimeCommitId.newBuilder()
                    .setCommitId(commit.timeId)
                    .build()
            }).build()
        }

enum class UpdateTimeCommitIdResult {
    MISSING_FIELDS,
    INVALID_TOKEN,
    INVALID_TIME_RANGE,
    INVALID_TIME_ZONE,
    INVALID_COMMIT_ID,
    OK
}

data class UpdateTimeCommitResult(val result: UpdateTimeCommitIdResult, val idBundle: Timecommit.TimeCommitIdBundle)
