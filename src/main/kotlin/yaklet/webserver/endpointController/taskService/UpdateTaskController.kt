package yaklet.webserver.endpointController.taskService

import dev.yakstack.transport.TaskOuterClass
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import yaklet.webserver.dbConn.DB_TASK_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.NoteHandler
import yaklet.webserver.handlers.TaskHandler
import yaklet.webserver.handlers.UserHandler
import java.util.*

class UpdateTaskController {
    private val userHandler = UserHandler()
    private val noteHandler = NoteHandler()
    private val taskHandler = TaskHandler()

    fun process(conn: DbConn, request: TaskOuterClass.Task): UpdateTaskResult {
        val userId = userHandler.userIdFromToken(conn, request.token)
        return when {
            isMissingCriticalFields(request) -> UpdateTaskResult(
                TaskIdResult.MISSING_FIELDS,
                TaskOuterClass.TaskId.getDefaultInstance()
            )
            userId.isEmpty() -> UpdateTaskResult(
                TaskIdResult.INVALID_TOKEN,
                TaskOuterClass.TaskId.getDefaultInstance()
            )
            !taskHandler.taskIdIsValid(conn, request.taskId) -> UpdateTaskResult(
                TaskIdResult.INVALID_TASK_ID,
                TaskOuterClass.TaskId.getDefaultInstance()
            )
            else -> UpdateTaskResult(
                TaskIdResult.OK,
                updateTask(conn, request, UUID.fromString(userId))
            )
        }
    }

    private fun updateTask(conn: DbConn, request: TaskOuterClass.Task, userId: UUID): TaskOuterClass.TaskId {
        conn.sql.update(table(DB_TASK_TB))
            .set(field("category"), request.category)
            .set(field("state"), request.state)
            .set(field("task"), request.task)
            .set(field("notes"), noteHandler.truncateNotes(request.notes))
            .where(
                field("task_id").eq(UUID.fromString(request.taskId)),
                field("user_id").eq(userId)
            ).execute()

        return TaskOuterClass.TaskId.newBuilder()
            .setTaskId(request.taskId)
            .build()
    }

    private fun isMissingCriticalFields(request: TaskOuterClass.Task): Boolean {
        return request.taskId.isEmpty() || request.task.isEmpty()
    }
}

enum class TaskIdResult {
    INVALID_TOKEN,
    MISSING_FIELDS,
    INVALID_TASK_ID,
    OK
}

class UpdateTaskResult(val result: TaskIdResult, val task: TaskOuterClass.TaskId)
