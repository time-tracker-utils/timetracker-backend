package yaklet.webserver.endpointController.taskService

import dev.yakstack.transport.TaskOuterClass
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import yaklet.webserver.dbConn.DB_TASK_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.NoteHandler
import yaklet.webserver.handlers.UserHandler
import java.util.*

class SubmitTaskController {
    private val userHandler = UserHandler()
    private val noteHandler = NoteHandler()

    fun process(conn: DbConn, request: TaskOuterClass.TaskBundle): SubmitTaskResult {
        val taskIdBundle = generateTaskIdBundle(request)
        val userId = userHandler.userIdFromToken(conn, request.token)
        return when {
            userId.isEmpty() -> SubmitTaskResult(
                TaskIdBundleResult.INVALID_TOKEN,
                TaskOuterClass.TaskIdBundle.getDefaultInstance())
            isMissingFields(request) -> SubmitTaskResult(
                TaskIdBundleResult.MISSING_FIELDS,
                TaskOuterClass.TaskIdBundle.getDefaultInstance()
            )
            else -> SubmitTaskResult(
                TaskIdBundleResult.OK,
                submitTasks(conn, request, taskIdBundle, UUID.fromString(userId)))
        }
    }

    private fun generateTaskIdBundle(request: TaskOuterClass.TaskBundle) = request
        .tasksList
        .map { UUID.randomUUID() }

    private fun isMissingFields(request: TaskOuterClass.TaskBundle): Boolean {
        request.tasksList.forEach {
            if (it.task.isEmpty()) { return true }
        }
        return false
    }

    private fun submitTasks(
        conn: DbConn,
        request: TaskOuterClass.TaskBundle,
        taskIdList: List<UUID>,
        userId: UUID
    ): TaskOuterClass.TaskIdBundle {
        // Ensure we are working with data-sets of the same size
        assert(request.tasksList.size == taskIdList.size)

        conn.sql.insertInto(
            table(DB_TASK_TB),
            field("task_id"),
            field("user_id"),
            field("category"),
            field("state"),
            field("task"),
            field("notes")
        ).apply {
            for ((idx, task) in request.tasksList.withIndex()) {
                values(
                    taskIdList[idx],
                    userId,
                    task.category,
                    task.state,
                    task.task,
                    noteHandler.truncateNotes(task.notes)
                )
            }
        }.execute()

        // Turn taskId list into a TaskIdBundle
        return TaskOuterClass.TaskIdBundle
            .newBuilder()
            .addAllTaskIds(
                taskIdList.map { TaskOuterClass.TaskId.newBuilder().setTaskId(it.toString()).build() }
            ).build()
    }
}

enum class TaskIdBundleResult {
    INVALID_TOKEN,
    MISSING_FIELDS,
    OK
}

data class SubmitTaskResult(val result: TaskIdBundleResult, val idBundle: TaskOuterClass.TaskIdBundle)
