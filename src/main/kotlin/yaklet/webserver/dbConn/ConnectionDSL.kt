package yaklet.webserver.dbConn

class ConnectionDSL(private val pool: ConnectionPool) {
    val conn = pool.getConnection()

    fun done() {
        pool.returnConnection(conn)
    }
}

// We need to make sure we catch all exceptions, otherwise we don't guarantee to return the
// connection at the end.
@Suppress("TooGenericExceptionCaught")
inline fun <T> withConn(pool: ConnectionPool, body: ConnectionDSL.() -> T) {
    val connDriver = ConnectionDSL(pool)
    try {
        connDriver.body()
    } catch (e: Exception) {
        throw e
    } finally {
        connDriver.done()
    }
}
