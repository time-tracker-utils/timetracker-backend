package yaklet.webserver.dbConn.connectionFactory

import yaklet.webserver.dbConn.ConnectionProvider
import java.sql.Connection

class SqlConnectionFactory: ConnectionFactory {
    override fun getConnection(): Connection = ConnectionProvider().conn
}

