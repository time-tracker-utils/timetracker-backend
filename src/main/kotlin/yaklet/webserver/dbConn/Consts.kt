package yaklet.webserver.dbConn

const val DB_NAME = "timedb"
const val DB_USER_TB = "users"
const val DB_UINFO_TB = "user_info"
const val DB_SESSIONS_TB = "sessions"
const val DB_TIME_COMMIT_TB = "timecommit"
const val DB_TASK_TB = "task"

const val EMAIL_LENGTH = 320
const val NAME_LENGTH = 255
const val DEVICE_NAME_LENGTH = 512
const val TZ_LENGTH = 255
