package yaklet.webserver.dbConn

import yaklet.webserver.config.DEFAULT_TIMEOUT
import yaklet.webserver.dbConn.connectionFactory.ConnectionFactory
import yaklet.webserver.dbConn.dslFactory.PostgresDSLFactory
import yaklet.webserver.messages.OUT_OF_CONNECTIONS_ERROR
import yaklet.webserver.messages.SHUTDOWN_MODE_MSG
import java.util.Stack
import java.util.logging.Level
import java.util.logging.Logger

class ConnectionPool(
    private val maxConnections: Int,
    private val connectionFactory: ConnectionFactory
) {
    @Volatile
    private var numActiveConnections: Int = 0
    @Volatile
    private var shutdownMode = false

    private val connections: Stack<DbConn> = Stack()

    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
        private const val TIME_TO_SLEEP = 100L
    }

    /**
     * Retrieves connection from the connection pool.
     * Synchronized to ensure retrieval and removal from connectionStack
     * behavior is predictable.
     *
     * NOTE: Possible optimization - doesn't necessarily need to be synchronized.
     * Locking and unlocking may be faster.
     *
     * Round robin type strategy where waiting for "slots" might be faster too.
     * Revisit if there are performance issues.
     *
     * @return Database connection
     */
    @Synchronized
    fun getConnection(): DbConn {
        if (shutdownMode) { throw ShutdownModeException(SHUTDOWN_MODE_MSG) }
        if (connections.empty() && numActiveConnections < maxConnections) {
            LOGGER.log(Level.INFO, "Requesting new connection: ${++numActiveConnections}")
            connections.push(DbConn(PostgresDSLFactory(connectionFactory.getConnection())))
        }
        var sleepCount = 0L
        while (connections.empty()) {
            sleepCount += TIME_TO_SLEEP
            Thread.sleep(TIME_TO_SLEEP)
            if (sleepCount > DEFAULT_TIMEOUT) {
                LOGGER.log(Level.SEVERE, "Max connections ($maxConnections) consumed. Timed out while serving user.")
                throw OutOfConnectionsException(OUT_OF_CONNECTIONS_ERROR)
            }
        }

        return connections.pop()
    }

    /**
     * Give back connection that was asked for.
     *
     * Note that we do not need to synchronize this method, because it doesn't matter what
     * order we get connections back.
     *
     * @param connection Connection to return
     */
    fun returnConnection(connection: DbConn) {
        connections.push(connection)
    }

    /**
     * Call on program exit. Enter shutdown mode.
     * Reap all active connections.
     */
    fun shutdown(wait: Long) {
        LOGGER.log(Level.INFO, "Begin connection reaping process.")
        LOGGER.log(Level.INFO, "Waiting for connections to finish executing.")
        shutdownMode = true

        var timeRested = 0L
        while (connections.size < numActiveConnections && timeRested < wait) {
            Thread.sleep(TIME_TO_SLEEP)
            timeRested += TIME_TO_SLEEP
        }

        if (timeRested >= wait) {
            LOGGER.log(Level.WARNING, "Unable to reclaim all connections.")
            LOGGER.log(Level.WARNING, "Reaping ${connections.size} of $numActiveConnections connections")
        } else {
            LOGGER.log(Level.INFO, "All connections reclaimed. Reaping connections.")
        }

        connections.forEach { it.sql.close() }
        LOGGER.log(Level.INFO, "Connections reaped.")
    }
}

data class OutOfConnectionsException(private val msg: String) : Exception() {
    override fun toString(): String = msg
}

data class ShutdownModeException(val msg: String) : Exception() {
    override fun toString(): String = msg
}
