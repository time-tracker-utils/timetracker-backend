package yaklet.webserver.dbConn.dslFactory

import org.jooq.DSLContext

interface DSLFactory {
    fun getDSL(): DSLContext
}
