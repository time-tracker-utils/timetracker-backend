package yaklet.webserver.handlers

import dev.yakstack.transport.Timecommit
import org.jooq.impl.DSL.field
import yaklet.webserver.dbConn.DB_TIME_COMMIT_TB
import yaklet.webserver.dbConn.DbConn
import java.lang.IllegalArgumentException
import java.time.ZoneId
import java.util.*

class TimeCommitHandler {
    private val zoneIds = ZoneId.getAvailableZoneIds()

    // In this case, we need to check to see if any of the values except for commitId have been unset.
    // Because of the number of fields, this conditional has to be somewhere.
    // You could store all of this in a boolean value and then do a conditional on that, but
    // it's the exact same thing which is stupid.
    @Suppress("ComplexCondition")
    fun missingCriticalFields(request: Timecommit.TimeCommitBundle): Boolean {
        request.commitsList.forEach {
            if (it.startTime == 0L
                || it.endTime == 0L
                || it.startTz.isEmpty()
                || it.endTz.isEmpty()
                || it.taskId.isEmpty()) {
                return true
            }
        }

        return false
    }

    fun timesAreValid(request: Timecommit.TimeCommitBundle): Boolean {
        request.commitsList.forEach {
            if (it.startTime > it.endTime) return false
        }
        return true
    }

     fun timeZonesAreValid(request: Timecommit.TimeCommitBundle) = zoneIds
        .containsAll(
            request.commitsList.map { it.startTz } + request.commitsList.map { it.endTz }
        )

    private fun commitIdIsValid(conn: DbConn, commitId: String): Boolean {
        return try {
            conn.sql
                .select(field("time_id"))
                .from(DB_TIME_COMMIT_TB)
                .where(field("time_id").eq(UUID.fromString(commitId)))
                .fetchOne(field("time_id"), String::class.java) != null
        } catch (_: IllegalArgumentException) {
            false
        }
    }

    // TODO Optimize
    fun commitsHaveValidCommitIds(conn: DbConn, commits: Timecommit.TimeCommitBundle): Boolean {
        commits.commitsList.forEach { commit ->
            if (!commitIdIsValid(conn, commit.timeId)) {
                return false
            }
        }
        return true
    }
}
