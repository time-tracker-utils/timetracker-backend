package yaklet.webserver

import yaklet.webserver.cli.Cli
import yaklet.webserver.config.DEFAULT_CONNECTIONS
import yaklet.webserver.dbConn.ConnectionPool
import yaklet.webserver.dbConn.ConnectionProvider
import yaklet.webserver.dbConn.connectionFactory.SqlConnectionFactory
import yaklet.webserver.server.Server

fun main(args: Array<String>) {
    System.getProperties().setProperty("org.jooq.no-logo", "true")
    val certPair = Cli(args).parse()
    ConnectionProvider().initTables()
    val connectionPool = ConnectionPool(DEFAULT_CONNECTIONS, SqlConnectionFactory())
    Server(connectionPool, certPair.first, certPair.second).start()
}
